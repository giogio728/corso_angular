import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

    @Input()
    public searchFilter: string = null;

    @Output()
    public searchRequested: EventEmitter<string> = new EventEmitter<string>();

    @Output()
    public clearRequested: EventEmitter<void> = new EventEmitter<void>();



    constructor() { }

    ngOnInit() {
    }

    public executeSearch() {
        console.log('Execute search clicked!');
        this.searchRequested.emit(this.searchFilter);
    }

    public clearSearch() {
        console.log('Clear search clicked!');
        this.clearRequested.emit();
    }

}
