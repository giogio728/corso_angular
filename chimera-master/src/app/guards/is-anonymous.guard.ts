import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionHolderService } from '../services/session-holder.service';

@Injectable({
    providedIn: 'root'
})
export class IsAnonymousGuard implements CanActivate {

    constructor(
        private sessionHolder: SessionHolderService,
        private router: Router) {
    }

    public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        console.log('Guard ' + IsAnonymousGuard.name + ' was invoked!');

        // Se NON sono autenticato, confermo
        if (!this.sessionHolder.isAuthenticated) {
            return true;
        }

        // Se sono autenticato, redirect alla home
        this.router.navigate(['']);
        return false;
    }
}
