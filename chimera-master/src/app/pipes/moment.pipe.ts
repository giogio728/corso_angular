import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
    name: 'moment'
})
export class MomentPipe implements PipeTransform {

    public transform(value: string, isFromNow: boolean, dateFormat: string): string {

        // Parse della data in stringa con moment
        const momentDate = moment(value);

        // Se ho una data relativa
        if (isFromNow) {
            return momentDate.fromNow();
        }

        // Se ho un formato specifico, applico quello
        if (!dateFormat) {
            dateFormat = 'YYYY-MM-DD HH:mm';
        }

        // Formattazione ed uscita
        return momentDate.format(dateFormat);
    }
}
