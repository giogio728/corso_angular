export const environment = {
  production: true,
  apiBaseUrl: 'https://chimera-authentication-api.azurewebsites.net/'
};
