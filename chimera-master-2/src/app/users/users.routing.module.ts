import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { IsAuthenticatedGuard } from '../guards/is-authenticated.guard';

const appRoutes: Routes = [
    { path: '', component: UsersListComponent, canActivate: [ IsAuthenticatedGuard ] },
    // { path: 'create', component: UserCreateComponent },
    { path: ':id/update', component: UserUpdateComponent, canActivate: [ IsAuthenticatedGuard ] },
    { path: ':id', component: UserDetailsComponent, canActivate: [ IsAuthenticatedGuard ] },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

// DONE Update => https://../users/32/update
// Create => https://../users/create
// DONE Details => https://../users/32
// DONE List => https://../users

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(appRoutes)
    ],
    exports: [ RouterModule ]
})
export class UsersRoutingModule { }
