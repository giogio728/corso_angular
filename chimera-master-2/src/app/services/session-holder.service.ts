import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { SignInContract } from '../models/sign-in-contract';
import { AuthenticationService } from './authentication.service';
import { Observable, Subject } from 'rxjs';

const LocalStorageKey = 'AUTH';

@Injectable({
    providedIn: 'root'
})
export class SessionHolderService {

    public isAuthenticated: boolean = false;
    public user: SignInContract = null;
    public password: string = null;

    constructor(
        private authentication: AuthenticationService) {
    }

    private setCredentials(localUser: SignInContract, localPassword: string) {

        // Salvo le credenziali nell'istanza corrente
        this.user = localUser;
        this.isAuthenticated = true;
        this.password = localPassword;

        // Creo struttura per conservazione
        const forStorage = {
            user: localUser,
            password: localPassword
        };

        // Converto in JSON
        const json = JSON.stringify(forStorage);

        // Imposto in local storage
        localStorage.setItem(LocalStorageKey, json);
    }

    public clearCredentials() {

        // Puliamo eventuali credenziali
        this.user = null;
        this.password = null;
        this.isAuthenticated = false;

        // Rimozione da local storage
        localStorage.removeItem(LocalStorageKey);
    }

    public tryRestoreCredentials() {

        // Cerco di estrarre da Local Storage
        const json = localStorage.getItem(LocalStorageKey);

        // Se non ho nulla, esco
        if (!json) {
            return;
        }

        // Conversione ad oggetto
        const fromStorage = JSON.parse(json);

        // Imposto le credenziali
        this.setCredentials(fromStorage.user, fromStorage.password);
    }

    public signIn(userName: string, password: string): Observable<SignInContract> {

        // Crea una observable non risolta e non rigettata
        const subject = new Subject<SignInContract>();

        this.authentication.signIn(userName, password).subscribe(

            // Success - 200 - OK
            (data: SignInContract) => {

                // 2) Salvo le credenziali in local storage
                this.setCredentials(data, password);

                // 3) Risolvo la Observable
                subject.next(data);
                subject.complete();
            },

            // Error
            (error) => {

                // 2) Puliamo il local storage
                this.clearCredentials();

                // 3) Rigettiamo l'observable
                subject.error(error);
                subject.complete();
            }
        );

        // Trasfromazione in observable
        return subject.asObservable();
    }

    public getHeaders() {

        // Unione dei segmenti
        const segments = this.user.userName + ':' + this.password;

        // Codifica base 64
        const b64 = btoa(segments);

        // Unione allo schema
        const token = 'Basic ' + b64;
        // ES: 'Basic bWF1cm86cGFzc3dvcmQ=';

        let httpHeaders = new HttpHeaders();
        httpHeaders = httpHeaders.append('Authorization', token);
        return httpHeaders;
    }
}
