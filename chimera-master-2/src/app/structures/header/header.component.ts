import { Component, OnInit } from '@angular/core';
import { SessionHolderService } from 'src/app/services/session-holder.service';
import { UserContract } from 'src/app/users/models/user-contract';
import { SignInComponent } from 'src/app/sign-in/sign-in.component';
import { SignInContract } from 'src/app/models/sign-in-contract';
import { ToastService } from 'src/app/shared/services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

    public isExpanded: boolean = false;

    constructor(
        public sessionHolder: SessionHolderService,
        private toast: ToastService,
        private router: Router) {
    }

    public toggleMenu() {

        // Se è aperta, collasso; se è chiusa, espando
        this.isExpanded = !this.isExpanded;
    }

    public signOut() {

        // Pulizia delle credenziali nel sessione holder e nel local storage
        this.sessionHolder.clearCredentials();

        // Emissione del messaggio di sign-out
        this.toast.success('Sign-out completatto con successo');

        // Ridirezionamento alla sign-in
        this.router.navigate(['sign-in']);
    }
}
