import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserContract } from '../models/user-contract';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SessionHolderService } from '../../services/session-holder.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
    constructor(
        private http: HttpClient,
        private sessionHolder: SessionHolderService) {
    }

    public updateUser(request: any): Observable<UserContract> {
        return this.http.post<UserContract>(
            environment.apiBaseUrl + 'api/Users/UpdateUser',
            request,
            { headers: this.sessionHolder.getHeaders() });
    }

    public fetchAllUsers(): Observable<UserContract[]> {

        return this.http.post<UserContract[]>(
            environment.apiBaseUrl + 'api/Users/FetchAllUsers',
            null,
            { headers: this.sessionHolder.getHeaders() });
    }

    public getUserByUserId(userId: string): Observable<UserContract> {

        // Creazione body
        const request = {
            userId
        };

        // Invoke remoto
        return this.http.post<UserContract>(
            environment.apiBaseUrl + 'api/Users/GetUserByUserId',
            request,
            { headers: this.sessionHolder.getHeaders() });
    }
}
