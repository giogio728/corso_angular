export class UserContract {
    public userId: string = null;
    public userName: string = null;
    public firstName: string = null;
    public lastName: string = null;
    public email: string = null;
    public isEnabled: boolean = null;
    public lastAccessDate: string = null;
    public isAdministrator: boolean = null;
}
