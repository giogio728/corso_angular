import { Component, OnInit } from '@angular/core';
import { UserBaseComponent } from '../common/user-base.component';
import { UsersService } from '../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../shared/services/toast.service';
import { UserContract } from '../models/user-contract';

@Component({
    selector: 'app-user-update',
    templateUrl: './user-update.component.html',
    styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent extends UserBaseComponent {

    constructor(
        activatedRoute: ActivatedRoute,
        router: Router,
        public usersService: UsersService,
        public toast: ToastService) {

        // Invocazione del costruttore base
        super(usersService, activatedRoute, toast, router);
    }

    public update() {

        // Creazione della request
        const request = {
            userId: this.userId,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            isEnabled: this.isEnabled,
        };

        // Busy e richiesta server
        this.isBusy = true;
        this.usersService.updateUser(request).subscribe(

            // Success
            (data: UserContract) => {
                this.isBusy = false;
                this.toast.success('User was updated with success!');
                this.router.navigate(['users']);
            },

            // Error
            (error: any) => {
                this.isBusy = false;
                // this.toast.warning('Provided data is invalid. Code:' + error.status + '...');
                this.toast.showBadRequest(error);
            }
        );
    }
}
