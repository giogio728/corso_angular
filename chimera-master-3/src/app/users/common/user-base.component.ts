import { UsersService } from '../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../shared/services/toast.service';
import { OnInit } from '@angular/core';
import { UserContract } from '../models/user-contract';

export abstract class UserBaseComponent implements OnInit {

    public isBusy: boolean = false;
    public userId: string = null;
    public userName: string = null;
    public firstName: string = null;
    public lastName: string = null;
    public email: string = null;
    public isEnabled: boolean = null;
    public lastAccessDate: string = null;
    public isAdministrator: boolean = null;

    constructor(
        public usersService: UsersService,
        public activatedRoute: ActivatedRoute,
        public toast: ToastService,
        public router: Router) {

        // Acquisizione del parametro definito come "id" nel routing
        this.activatedRoute.params.forEach(par => this.userId = par.id);
        console.log('param ' + this.userId);
    }

    public ngOnInit() {

        // Dopo il caricamento del componente, richiamo il server
        this.getUserById();
    }

    public getUserById() {

        // Chiamata HTTP per il recupero dei dati
        this.isBusy = true;
        this.usersService.getUserByUserId(this.userId).subscribe(

            // Success
            (data: UserContract) => {

                // Remapping delle proprietà
                this.userName = data.userName;
                this.firstName = data.firstName;
                this.lastName = data.lastName;
                this.isEnabled = data.isEnabled;
                this.isAdministrator = data.isAdministrator;
                this.lastAccessDate = data.lastAccessDate;
                this.email = data.email;

                // Rimozione del busy indicator
                this.isBusy = false;
            },

            // Error
            (error: any) => {

                // Rimozione del busy indicator
                this.isBusy = false;

                // Differenzio a seconda della risposta
                if (error.statusCode === 404) {
                    this.toast.warning('Requested user was not found!');
                    this.router.navigate(['users']);
                    return;
                }
                if (error.statusCode === 403) {
                    this.toast.warning('Requested user is not allowed!');
                    this.router.navigate(['users']);
                    return;
                }
                if (error.statusCode === 401) {
                    this.router.navigate(['sign-in']);
                    return;
                }

                // In tutti gli altri casi errore e home
                this.toast.error('Server error: ' + error.statusCode);
                this.router.navigate(['']);
            }
        );
    }
}
