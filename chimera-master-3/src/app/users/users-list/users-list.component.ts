import { Component, OnInit } from '@angular/core';
import { UserContract } from '../models/user-contract';
import { UsersService } from '../services/users.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

    public isBusy = false;
    public users: UserContract[] = [];

    public filteredUsers: UserContract[] = [];

    // Questa non serve per questa implementazione
    public filter: string = null;

    constructor(
        private usersService: UsersService) {
    }

    public ngOnInit() {

        // Lancio il caricamento utenti da server
        this.fetch();
    }

    public fetch() {

        console.log('Avvio caricamento...');
        this.isBusy = true;
        this.usersService.fetchAllUsers().subscribe(
            (data: UserContract[]) => {
                this.isBusy = false;
                this.users = data;

                // Assegno alla lista filtrata
                this.filteredUsers = this.users;
            },
            (error: any) => {
                this.isBusy = false;
            }
        );
    }

    public searchOnChildRequested(filterOnChild: string) {

        // Non devo filtrare se non ho valori nel filtro
        if (!filterOnChild) {
            // this.clearFilter();
            return;
        }

        // Filtro minuscolo
        const lowercaseFilter = filterOnChild.toLowerCase();

        this.filteredUsers = _.filter(this.users, user =>
            (user.userName != null  &&
            user.userName.toLowerCase().indexOf(lowercaseFilter) >= 0) ||
            (user.firstName != null  &&
            user.firstName.toLowerCase().indexOf(lowercaseFilter) >= 0) ||
            (user.lastName != null  &&
            user.lastName.toLowerCase().indexOf(lowercaseFilter) >= 0)
        );

    }

    public clearOnChildRequested() {

        // Ripristino la collezione originale
        this.filteredUsers = this.users;
    }
}
