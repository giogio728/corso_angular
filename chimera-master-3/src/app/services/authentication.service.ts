import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SignInContract } from '../models/sign-in-contract';
import { PwaService } from './pwa.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    constructor(
        private httpClient: HttpClient,
        private pwaService: PwaService) { }

    public signIn(userName: string, password: string): Observable<SignInContract> {

        const request = {
            userName,
            password
        };

        return this.httpClient.post<SignInContract>(
            environment.apiBaseUrl + 'api/Authentication/SignIn',
            request);
    }
}
