import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { ToastService } from '../shared/services/toast.service';

@Injectable({
    providedIn: 'root'
})
export class PwaService {

    public deferredPrompt: any = null;
    public updateIsAvailable = false;

    constructor(
        private swUpdate: SwUpdate,
        private toast: ToastService) {

        // Attach event on update availabled
        console.log('Registering "updateAvailable" event on service worker...');
        swUpdate.available.subscribe(event => {

            // Show info to user
            this.toast.info('An update is avalabled: please reload app to update...');

            // Set flag for update
            console.log('Event "updateAvailable" fired!');
            this.updateIsAvailable = true;
        });

        // Attach window event
        console.log('Registering "beforeinstallprompt" event on window...');
        window.addEventListener('beforeinstallprompt', (event) => {

            // Prevent Chrome 67 and earlier from automatically showing the prompt
            console.log('Event "beforeinstallprompt" fired!');
            event.preventDefault();

            // Stash the event so it can be triggered later.
            this.deferredPrompt = event;
        });
    }

    /**
     * Checks if update is available
     */
    public canUpdate(): boolean {

        // Check variable
        return this.updateIsAvailable;
    }

    /**
     * Executes update of current app
     */
    public executeUpdate(): void {

        // Simple reload of window
        window.location.reload();
    }

    /**
     * Checks if application can be installed as app
     */
    public canInstall(): boolean {

        // Can install if we have event
        return this.deferredPrompt != null;
    }

    /**
     * Execute install as app
     */
    public executeInstall(): void {

        // Show browser prompt
        console.log('Showing prompt for install as app...');
        this.deferredPrompt.prompt();

        // Wait for the user to respond to the prompt
        this.deferredPrompt.userChoice.then(choiceResult => {

            // If prompt is accepted
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
            } else {
                console.log('User dismissed the A2HS prompt');
            }

            // Remove reference to install prompt
            this.deferredPrompt = null;
        });
    }
}
