import { Component, OnInit } from '@angular/core';
import { SessionHolderService } from '../services/session-holder.service';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SampleDialogComponent } from '../sample-dialog/sample-dialog.component';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    public authenticatedUser: string = null;

    constructor(
        private modalService: NgbModal,
        private sessionHolder: SessionHolderService) {
    }

    public ngOnInit() {
        this.authenticatedUser = this.sessionHolder.user.userName;
    }

    public openDialog() {

        // Apertura del component in modale e recupero del suo riferimento
        const referenceToModal: NgbModalRef = this.modalService.open(SampleDialogComponent);

        // Aggancio il "result" sul componente figlio
        referenceToModal.result.then(
            reason => {
                if (reason == null) {
                    console.log('Dialogo annullato');
                } else {
                    console.log('Dialogo chiuso con conferma:' + reason);
                }
            }
            ,
            (reason) => {
                if (reason === ModalDismissReasons.BACKDROP_CLICK) {
                    console.log('Dialogo chiuso con click sulla ombra');
                } else if (reason === ModalDismissReasons.ESC) {
                    console.log("Dialogo chiuso con ESC");
                }
            }
        );


    }
}
