import { Injectable } from '@angular/core';
import * as toastr from 'toastr';

@Injectable()
export class ToastService {
    public showBadRequest(response: any): any {

        // Prelevo la proprietà "error"
        const error: any = response.error;

        // Elenco di messaggi di errore
        const messages: string[] = [];

        // Iterazione su tutte le proprietà di "error"
        for (const propertyName in error) {

            // Verifico se è una proprietà reale
            if (!error.hasOwnProperty(propertyName)) {
                continue;
            }

            // Recupero il valore della proprietà corrente
            const propValue = error[propertyName];

            // Scorro i messaggi nella proprietà
            for (let i = 0; i < propValue.length; i++) {
                messages.push(propValue[i]);
            }
        }

        const allMessages = messages.join('<br>');

        this.warning(allMessages);

        // {
        //     "":[
        //         "User 'Administrator' cannot be modified because is a platform administrator"
        //     ],
        //     "email": [
        //         "L'email specificata non è valida",
        //         "L'email è troppo lunga"
        //     ]
        // }
    }
    public info(message: string): void {
        toastr.info(message);
    }
    public warning(message: string): void {
        toastr.warning(message);
    }
    public success(message: string): void{
        toastr.success(message);
    }
    public error(message: string): void {
        toastr.error(message);
    }


}
