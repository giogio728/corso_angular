export class SignInContract {
    public userId: string = null;
    public userName: string = null;
    public email: string = null;
    public firstName: string = null;
    public lastName: string = null;
    public lastAccessDate: string = null;
}

//https://chimera-authentication-api.azurewebsites.net/swagger
