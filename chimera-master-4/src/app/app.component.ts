import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { NullTemplateVisitor } from '@angular/compiler';
import { SessionHolderService } from './services/session-holder.service';
import { SwUpdate } from '@angular/service-worker';
import { ToastService } from './shared/services/toast.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

    constructor(
        private translate: TranslateService,
        private sessionHolder: SessionHolderService) {
    }

    public ngOnInit(){
        this.translate.setDefaultLang('en');
        this.sessionHolder.tryRestoreCredentials();
    }

}
