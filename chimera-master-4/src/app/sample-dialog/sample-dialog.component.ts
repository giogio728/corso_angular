import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-sample-dialog',
    templateUrl: './sample-dialog.component.html',
    styleUrls: ['./sample-dialog.component.css']
})
export class SampleDialogComponent implements OnInit {

    constructor(
        private activeModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    public cancel() {
        this.activeModal.close(null);
    }

    public create() {
        this.activeModal.close('Test');
    }
}
