import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionHolderService } from '../services/session-holder.service';

@Injectable({
    providedIn: 'root'
})
export class IsAuthenticatedGuard implements CanActivate {

    constructor(
        private sessionHolder: SessionHolderService,
        private router: Router) {
    }

    public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        // Se sono autenticato, confermo ed esco
        if (this.sessionHolder.isAuthenticated) {
            return true;
        }

        // Non confermo accesso
        this.router.navigate(['sign-in']);
        return false;
    }

}
