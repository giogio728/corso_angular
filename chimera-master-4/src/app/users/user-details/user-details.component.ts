import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/users/services/users.service';
import { UserContract } from 'src/app/users/models/user-contract';
import { ToastService } from 'src/app/shared/services/toast.service';
import { UserBaseComponent } from '../common/user-base.component';

@Component({
    selector: 'app-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent extends UserBaseComponent {

    constructor(
        activatedRoute: ActivatedRoute,
        router: Router,
        public usersService: UsersService,
        public toast: ToastService) {

        // Invocazione del costruttore base
        super(usersService, activatedRoute, toast, router);
    }
}
