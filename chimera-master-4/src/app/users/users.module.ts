import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersRoutingModule } from './users.routing.module';

//#region FontAwesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
library.add(far, fas);
//#endregion

import { MomentPipe } from '../pipes/moment.pipe';
import { UserUpdateComponent } from './user-update/user-update.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { SearchComponent } from './partials/search/search.component';

@NgModule({
    declarations: [
        UsersListComponent,
        UserDetailsComponent,
        UserUpdateComponent,
        MomentPipe,
        SearchComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        UsersRoutingModule,
        FontAwesomeModule,
        SharedModule
    ]
})
export class UsersModule { }
