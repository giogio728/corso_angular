import { Component, OnInit } from '@angular/core';
import { PwaService } from 'src/app/services/pwa.service';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

    constructor(
        public pwaService: PwaService) {
    }

    ngOnInit() {
    }
}
