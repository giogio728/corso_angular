import { Component, OnInit } from '@angular/core';
import { SessionHolderService } from './services/session-holder.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

    constructor(private sessionHolder: SessionHolderService) {
    }

    public ngOnInit() {
        this.sessionHolder.tryRestoreCredentials();
    }
}
