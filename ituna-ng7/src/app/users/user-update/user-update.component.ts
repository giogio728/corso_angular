import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../shared/services/toast.service';
import { UserBaseComponent } from '../common/user-base.component';
import { UserContract } from '../models/user-contract';

@Component({
    selector: 'app-user-update',
    templateUrl: './user-update.component.html',
    styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent extends UserBaseComponent {

    constructor(
        activatedRoute: ActivatedRoute,
        router: Router,
        public userService: UsersService,
        public toast: ToastService
    ) {
        // invocazione del costruttore di base
        super(userService, activatedRoute, toast, router);
    }

    public update() {
        // creazione della request
        const request = {
            userId: this.userId,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            isEnabled: this.isEnabled
        };

        // Busy a richiesta server
        this.isBusy = true;
        this.userService.updateUser(request).subscribe(

            // Success
            (data: UserContract) => {
                this.isBusy = false;
                this.toast.success('User was updated with success');
                this.router.navigate(['users']);
            },

            // Error
            (error: any) => {
                this.isBusy = false;
                // this.toast.warning('Provided data is invalid(code: ' + error.statusCode + '...');
                this.toast.showBadRequest(error);
            }
        );
    }

}
