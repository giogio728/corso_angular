import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { UsersRoutingModule } from '../users/users.routing.module';
import { UsersListComponent } from './users-list/users-list.component';

//#region FontAwesome

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { MomentPipe } from '../pipes/moment.pipe';
import { UserUpdateComponent } from './user-update/user-update.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { SearchComponent } from '../users/partials/search/search.component';
// Aggiunta di FreeRegular e FreeSolid alla libreria
library.add(far, fas);

//#endregion



@NgModule({
  declarations: [
      UsersListComponent,
      UsersDetailComponent,
      MomentPipe,
      UserUpdateComponent,
      SearchComponent
    ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FontAwesomeModule,
    FormsModule,
    SharedModule,
  ]
})
export class UsersModule { }
