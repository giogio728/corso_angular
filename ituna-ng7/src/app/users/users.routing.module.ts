import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { UserUpdateComponent } from './user-update/user-update.component';


const appRoutes: Routes = [
    { path: '', component: UsersListComponent },
    // {path: 'create', component: UserCreateComponent },
    { path: ':id', component: UsersDetailComponent }, // i 2 punti indicano l'esistenza di un parametro
    {path: ':id/update', component: UserUpdateComponent},
    // {path: 'categories/:id/products/:key', component: NomeComponente },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

// Udpate => https://../users/32/update
// Details => https://../users/32
// Create => https://../users/create
// List => https://../users

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }
