import { Component, OnInit } from '@angular/core';
import { UserContract } from '../models/user-contract';
import { UsersService } from '../services/users.service';

// Could not find a declaration file for moudule 'lodash' => vuol dire semplicemente che non ci sono ancora i types
import * as _ from 'lodash';

@Component({
    selector: 'app-user-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

    public isBusy = false;
    public users: UserContract[] = [];
    public filter: string = null;
    public filteredUsers: UserContract[] = [];

    constructor(
        private userService: UsersService,
    ) { }

    ngOnInit() {

        // lancio il caricamento utenti da server
        this.fetch();
    }

    public fetch() {

        console.log('fetch called');

        this.isBusy = false;
        this.userService.fetchAllUsers().subscribe(
            (data: UserContract[]) => {
                this.isBusy = false;
                this.users = data;

                // Assegno alla lista filtrata
                this.filteredUsers = this.users;
            },
            (error: any) => {
                this.isBusy = false;
            });
    }

    public clearFilter() {
        this.filter = null;
        this.filteredUsers = this.users;
    }

    public search() {
        // Non devo filtrare se non ho valori nel filtro
        if (!this.filter) {
            this.clearFilter();
            return;
        }

        // Filtro minuscolo
        const lowercaseFilter = this.filter.toLowerCase();

        this.filteredUsers = _.filter(this.users, user =>
            user.userName != null &&
            user.userName.toLowerCase().indexOf(lowercaseFilter) >= 0 ||
            user.firstName != null &&
            user.firstName.toLowerCase().indexOf(lowercaseFilter) >= 0 ||
            user.lastName != null &&
            user.lastName.toLowerCase().indexOf(lowercaseFilter) >= 0
        );
    }

    public searchOnChildRequested($event) {
        console.log('Evento \'searchRequested\' sollevato sul figlio');
    }

    public clearOnChildRequested($event) {
        console.log('Evento \'clearRequested\' sollevato sul figlio');
    }
}
