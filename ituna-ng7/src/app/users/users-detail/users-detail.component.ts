import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../services/users.service';
import { UserContract } from '../models/user-contract';
import { ToastService } from '../../shared/services/toast.service';

@Component({
    selector: 'app-user-detail',
    templateUrl: './users-detail.component.html',
    styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit {

    public isBusy: boolean = false;
    public userId: string = null;
    public userName: string = null;
    public firstName: string = null;
    public lastName: string = null;
    public email: string = null;
    public isEnabled: boolean = false;
    public lastAccessDate: string = null;
    public isAdministrator: boolean = false;

    // route attualmente attiva per la route corrente
    constructor(
        private usersService: UsersService,
        private activatedRoute: ActivatedRoute,
        private toast: ToastService,
        private router: Router

    ) {
        // acquisizione del parametro definito
        this.activatedRoute.params.forEach(par => this.userId = par.id);
        console.log('param' + this.userId);
    }

    // questo scatta subito dopo il costruttore
    public ngOnInit() {
        // Dopo il caricamento del componente, richiamo il server
        this.getUserById();
    }

    public getUserById() {
        this.isBusy = true;
        this.usersService.getUserByUserId(this.userId).subscribe(

            // Success
            (data: UserContract) => {

                // Remapping delle proprietà
                this.userName = data.userName;
                this.firstName = data.firstName;
                this.lastName = data.lastName;
                this.isEnabled = data.isEnabled;
                this.isAdministrator = data.isAdministrator;
                this.lastAccessDate = data.lastAccessDate;
                this.email = data.email;
                this.isBusy = false;
            },

            // Error
            (error: any) => {

                // rimozione del busy indicator
                this.isBusy = false;

                // Differenzio a seconda della risposta
                if (error.statusCode === 404) {
                    this.toast.warning('Requested user was not found');
                    return;
                }
                if (error.statusCode === 403) {
                    this.router.navigate(['users']);
                    this.toast.warning('Requested user is not allowed!');
                    return;
                }
                if (error.statusCode === 401) {
                    this.router.navigate(['sign-in']);
                    this.toast.warning('Requested user is not allowed!');
                    return;
                }

                this.toast.error(`Server error: ${error.statusCode}`);
                this.router.navigate(['']);
            }
        );
    }
}
