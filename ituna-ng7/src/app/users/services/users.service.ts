import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserContract } from '../models/user-contract'
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SessionHolderService } from 'src/app/services/session-holder.service';

@Injectable({
    providedIn: 'root'
})
export class UsersService {



    constructor(
        private http: HttpClient,
        private sessionHolder: SessionHolderService
    ) { }

    public fetchAllUsers(): Observable<UserContract[]>{
        return this.http.post<UserContract[]>(
            `${environment.apiBaseUrl}api/Users/FetchAllUsers`,
            null,
            { headers: this.sessionHolder.getHeaders() });
    }

    public getUserByUserId(userId: string): Observable<UserContract> {

        // Creazione Body
        const request = {
            userId
        };

        // Invoke remote
        return this.http.post<UserContract>(
            environment.apiBaseUrl + 'api/Users/GetUserByUserId',
            request,
            { headers: this.sessionHolder.getHeaders() }
            );
    }

    public updateUser(request: any): Observable<UserContract> {
        return this.http.post<UserContract>(
            `${environment.apiBaseUrl}api/Users/UpdateUser`,
            request,
            { headers: this.sessionHolder.getHeaders() }
            );

        // return this.http.post<UserContract[](
        //     `${environment.apiBaseUrl}api/Users/UpdateUser`,
        //     request,
        //     { headers: this.sessionHolder.getHeaders() });
    }
}
