import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { CommonModule } from '@angular/common';
import { IsAuthenticatedGuard } from './guards/is-authenticated.guard';
import { IsAnonymousGuard } from './guards/is-anonymous.guard';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [IsAuthenticatedGuard] },
    { path: 'sign-in', component: SignInComponent, canActivate: [IsAnonymousGuard] },
    { path: 'users', loadChildren: './users/users.module#UsersModule' },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
