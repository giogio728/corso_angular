import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { SignInContract } from '../models/sign-in-contract';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    constructor(private httpClient: HttpClient) {
    }

    public signIn(userName: string, password: string): Observable<SignInContract> {
        const url: string = environment.apiBaseUrl;
        const partialUrl: string = 'api/Authentication/SignIn';
        const fullUrl: string = `${url}${partialUrl}`;

        const body = {
            userName,
            password
        };

        // l'http client mi ritorna un observable
        return this.httpClient.post<SignInContract>(fullUrl, body);
    }
}
