import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { SignInContract } from '../models/sign-in-contract';
import { AuthenticationService } from './authentication.service';
import { Observable, Subject } from 'rxjs';

const LocalStorageKey = 'AUTH';

@Injectable({
    providedIn: 'root'
})
export class SessionHolderService {

    public isAuthenticated: boolean = false;
    public user: SignInContract = null;
    public password: string = null;


    constructor(
        private authentication: AuthenticationService) {
    }


    private setCredentials(user: SignInContract, password: string) {
        // salvo le credenziali nell'istanza corrente
        this.user = user;
        this.isAuthenticated = true;
        this.password = password;

        // Creo struttura per conservazione
        const forStorage = {
            user,
            password
        };

        const json = JSON.stringify(forStorage);

        // Imposto in local storage
        localStorage.setItem(LocalStorageKey, json);
    }


    private clearCredentials() {
        // puliamo eventuali credenziali
        this.user = null;
        this.password = null;
        this.isAuthenticated = false;

        // puliamo il local storage
        localStorage.removeItem(LocalStorageKey);
    }


    public tryRestoreCredentials() {
        // Cerco di estrarre da Local Storage
        const json = localStorage.getItem(LocalStorageKey);

        // Se non ho nulla, esco
        if (!json) {
            return;
        }

        // Conversione ad oggetto
        const fromStorage = JSON.parse(json);

        // imposto le credenziali
        this.setCredentials(fromStorage.user, fromStorage.password);
    }


    public signIn(userName: string, password: string): Observable<SignInContract> {

        // // Al posto che fare sta roba con le promise nestate usiamo il subject
        // //  https://blog.angularindepth.com/rxjs-understanding-subjects-5c585188c3e1
        // return new Promise((resolve, reject) => {

        //     return new Promise((res2, rej2) => {
        //         resolve("dati emessi");
        //     })
        //     .then((data)) =>
        //     {
        //         resolve(data);
        //     })
        // });

        // Crea un observable non risolta e non rigettata
        const subject = new Subject<SignInContract>();

        this.authentication.signIn(userName, password).subscribe(

            // Success - 200 - OK
            (data: SignInContract) => {
                // salvo le credenziali in local storage
                this.setCredentials(data, password);

                // risolvo la Observable
                subject.next(data);
                subject.complete();
            },

            // Error
            (error) => {
                // self-explanatory
                this.clearCredentials();

                // rigettiamo l'observable
                subject.error(error);
                subject.complete();
            }
        );

        return subject.asObservable();
    }


    public getHeaders() {
        // Unione dei segmenti
        const segments = `${this.user.userName}:${this.password}`;

        // codifica base 64
        const b64 = btoa(segments); // btoa significa binary to array

        // Unione allo schema
        const token = `Basic ${b64}`;
	// ES: 'Basic bWF1cm86cGFzc3dvcmQ=';

        let httpHeaders = new HttpHeaders();
        httpHeaders = httpHeaders.append('Authorization', token);
        return httpHeaders;
    }
}
