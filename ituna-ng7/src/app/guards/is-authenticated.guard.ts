import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionHolderService } from '../services/session-holder.service';

@Injectable({
    providedIn: 'root'
})
export class IsAuthenticatedGuard implements CanActivate {

    constructor(
        private sessionHolder: SessionHolderService,
        private router: Router) {

    }

    public canActivate(
        next: ActivatedRouteSnapshot, // informazioni della route a cui state per andare
        state: RouterStateSnapshot // stato corrente del router
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        // se sono autenticato, confermo ed esco
        if (this.sessionHolder.isAuthenticated) {
            return true;
        }

        // non confermo accesso
        this.router.navigate(['sign-in']);
        return false;
    }
}
