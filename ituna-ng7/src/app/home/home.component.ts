import { Component, OnInit } from '@angular/core';
import { SessionHolderService } from '../services/session-holder.service';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    public authenticatedUser: string = null;

    constructor(

        private sessionHolder: SessionHolderService
    ) { }

    public ngOnInit() {
        this.authenticatedUser = this.sessionHolder.user.userName;
    }
}
