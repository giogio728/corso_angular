import { Injectable } from '@angular/core';

// stiamo forzatamente creando un alias con cui possiamo accedere all'oggetto toast
import * as toastr from 'toastr';

@Injectable()
export class ToastService {
    public showBadRequest(response: any): any {
        // Prelevo la proprietà error
        const error: any = response.error;

        // Elenco di messaggi di errore
        const messages: string[] = [];

        // iterazione su tutte le proprietà di "error"
        for (const propertyName in error) {
            // verifico se è una proprietà reale
            if (!error.hasOwnProperty(propertyName)) {
                continue;
            }

            // recupero il valore della proprietà corrente
            const propValue = error[propertyName];

            // // Scorro i messagi nella proprietà
            // for (const prop of propValue) {
            //     messages.push(prop);
            // }

            for (let i = 0; i < propValue.length; i++) {
                messages.push(propValue[i]);
            }
        }

        const allMessages = messages.join('<br>');

        this.warning(allMessages);
    }
    public info(message: string): void {
        toastr.info(message);
    }
    public warning(message: string): void {
        toastr.warning(message);
    }
    public success(message: string): void {
        toastr.success(message);
    }
    public error(message: string): void {
        toastr.error(message);
    }
}
