import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

import { ToastService } from '../shared/services/toast.service';
import { SessionHolderService } from '../services/session-holder.service';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

    public isBusy: boolean = false;
    public userName: string = 'mauro';
    public password: string = 'password';


    constructor(
        private sessionHolder: SessionHolderService,
        private router: Router,
        private toast: ToastService) {
    }


    public ngOnInit(): void {
        // Se sono autenticato, vado a home
        if (this.sessionHolder.isAuthenticated) {
            this.router.navigate(['']);
            return;
        }
        console.log('Questo non dovrebbe essere chiamato');
    }



    public signIn() {
        // 1) metto is busy a true
        this.isBusy = true;

        // 2) Chiamata HTTP verso il server
        // promise
        // this.authentication.signIn(this.userName, this.password).then(
        //     (data) => {

        //     },
        //     (error) => {

        //     }
        // );

        this.sessionHolder.signIn(this.userName, this.password)
            // Attesa di risposta
            .subscribe(

                // 4.A) Risposta OK
                (data) => {
                    // 5) Rimuovo isBusy
                    this.isBusy = false;

                    // 6) Mostro feedback utente (ok o failed)
                    this.toast.success(`Benvenuto ${data.firstName}!`);

                    // 7) navigazione a home page (se ok)
                    this.router.navigate(['']);

                },
                // 4.B) Risposta Fallita
                (error) => {
                    // 5) Rimuovo isBusy
                    this.isBusy = false;

                    // 6) Mostro feedback utente (ok o failed)
                    this.toast.warning('Fallito!');
                }
            );
    }
}
